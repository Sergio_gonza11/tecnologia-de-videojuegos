﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_7
{
    class Program
    {
        static int []invertir(int [] arrayoriginal)
        {
            int tam = arrayoriginal.Length;
            int[] arrayinvertido =arrayoriginal;
            for (int pos = 0; pos < tam/2; pos++)
            {
                int temp = arrayoriginal[pos];
                arrayoriginal[pos] = arrayinvertido[tam - pos - 1];

                arrayinvertido[tam - pos - 1] = temp;

            }
            return arrayinvertido;
        } 

        static void Main(string[] args)
        {
            int[] arrayoriginal = { 4, 6, 8, 9 };
            int tam = arrayoriginal.Length;
            for (int pos = 0; pos < tam; pos++)
            {
                Console.WriteLine(arrayoriginal[pos]);
            }
                int[] arrayinverso = invertir(arrayoriginal);
            for (int pos = 0; pos < tam; pos++)
            {
                Console.WriteLine(arrayinverso[pos]);
                
            }
            
            
            Console.ReadLine();

        }
    }
}
