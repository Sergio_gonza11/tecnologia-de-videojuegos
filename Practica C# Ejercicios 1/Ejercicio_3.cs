﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserte su nota");
            float nota= 
                float.Parse(Console.ReadLine());
            if (nota >= 9.0)
                Console.WriteLine("Sobresaliente");
            else
                if (nota<5.0)
                Console.WriteLine("Suspenso");
            else
                if (nota == 6.0)
                Console.WriteLine("Bien");
            else
                if (nota == 5.0)
                Console.WriteLine("Suficiente");
            else
                Console.WriteLine("Notable");

            Console.ReadKey();
        }
    }
}
