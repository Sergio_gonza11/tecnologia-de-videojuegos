﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("inserte un año");
            int anio =
                int.Parse(Console.ReadLine());

            if ((anio % 4 == 0) && (anio % 100 != 0))
                Console.WriteLine("es bisiesto");
            else
                if (anio % 400 == 0)
                Console.WriteLine("es bisiesto");
            else
                Console.WriteLine("no es bisiesto");
            Console.ReadLine();
        }
    }
}
